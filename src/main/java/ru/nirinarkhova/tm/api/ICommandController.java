package ru.nirinarkhova.tm.api;

public interface ICommandController {

    void exit();

    void showAbout();

    void showVersion();

    void showCommands();

    void showArguments();

    void showHelp();

    void showSystemInfo();

}
