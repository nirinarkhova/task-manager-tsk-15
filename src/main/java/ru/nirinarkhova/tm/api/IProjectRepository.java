package ru.nirinarkhova.tm.api;

import ru.nirinarkhova.tm.model.Project;

import java.util.Comparator;
import java.util.List;

public interface IProjectRepository {

    List<Project> findAll();

    List<Project> findAll(Comparator<Project> comparator);

    void add(Project project);

    void remove(Project project);

    void clear();

    Project findOneById(String id);

    Project removeOneById(String id);

    Project findOneByName(String name);

    Project findOneByIndex(Integer index);

    Project removeOneByIndex(Integer index);

    Project removeOneByName(String name);

}
