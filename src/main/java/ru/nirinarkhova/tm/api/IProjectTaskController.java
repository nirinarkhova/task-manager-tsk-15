package ru.nirinarkhova.tm.api;

import ru.nirinarkhova.tm.exception.entity.ProjectNotFoundException;
import ru.nirinarkhova.tm.exception.entity.TaskNotFoundException;

public interface IProjectTaskController {


    void showTaskByProjectId() throws ProjectNotFoundException;

    void bindTaskByProject() throws TaskNotFoundException, ProjectNotFoundException;

    void unbindTaskFromProject() throws TaskNotFoundException;

    void removeProjectById() throws ProjectNotFoundException;

}
