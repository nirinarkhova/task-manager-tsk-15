package ru.nirinarkhova.tm.api;

import ru.nirinarkhova.tm.model.Task;

import java.util.Comparator;
import java.util.List;

public interface ITaskRepository {

    List<Task> findAll();

    Task findOneByProjectId(String projectId);

    List<Task> findAllByProjectId(String projectId);

    void removeAllByProjectId(String projectId);

    Task bindTaskByProject(String taskId, String projectId);

    Task unbindTaskByProject(String projectId);

    List<Task> findAll(Comparator<Task> comparator);

    void add(Task task);

    void remove(Task task);

    void clear();

    Task findOneById(String id);

    Task removeOneById(String id);

    Task findOneByName(String name);

    Task findOneByIndex(Integer index);

    Task removeOneByIndex(Integer index);

    Task removeOneByName(String name);

}
