package ru.nirinarkhova.tm.comparator;

import ru.nirinarkhova.tm.api.entity.IHasName;

import java.util.Comparator;

public class ComparatorByName implements Comparator<IHasName> {

    private static final ComparatorByName INSTANSE = new ComparatorByName();

    private ComparatorByName() {
    }

    public static ComparatorByName getInstance() {
        return INSTANSE;
    }

    @Override
    public int compare(final IHasName o1, final IHasName o2) {
        if(o1 == null || o2 == null) return 0;
        return o1.getName().compareTo(o2.getName());
    }

}
