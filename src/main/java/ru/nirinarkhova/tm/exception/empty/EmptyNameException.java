package ru.nirinarkhova.tm.exception.empty;

public class EmptyNameException extends Exception {

    public EmptyNameException() {
        super("Error! Name is empty...");
    }

}
